Name:           fastback
Version:        2
Release:        2
Summary:        File uploader, configureable file uploader

Group:          System Environment/Base
License:        GPLv2+
URL:            http://fedorahosted.org/fastback
Source0:        fastback-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: openssl-devel
Requires: openssl

%if 0%{?rhel}
%if 0%{?rhel} <= 5
BuildRequires: curl-devel
%else
BuildRequires: libcurl-devel
%endif
%else
BuildRequires: libcurl-devel
%endif

%description
Fastback is a command line tool to upload files to a ticketing system, or
other configurable URL (FTP,SCP,...).

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README FIXME
/bin/fastback
/bin/fastback-unload-receipt
%attr(0644,root,root) %config(noreplace) /etc/fastback.conf


%changelog
* Mon Sep  2 2009 Gavin Romig-Koch <gavin@redhat.com> 2-2
- clean up documentation
- clean up spec file (for rpmlint)

* Mon Aug 17 2009 Gavin Romig-Koch <gavin@redhat.com> 2-1
- added support for SCP and HTTP
- improved the documentation
- improved the error handling

* Mon Aug 17 2009 Gavin Romig-Koch <gavin@redhat.com> 0.1-1
- Initial version.

