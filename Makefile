
NAME=fastback
VERSION=2

ifndef DESTDIR
DESTDIR=/
endif


#
# RPM specific setup stuff
# 

default: fastback

fastback: fastback.o
	g++ -g -o $@ $$(curl-config --libs) $< 

fastback.o: fastback.cpp
	g++ -g -c -o $$(curl-config --cflags) $@ $<

install:
	install -d $(DESTDIR)/bin/ $(DESTDIR)/etc
	install fastback fastback-unload-receipt $(DESTDIR)/bin/
	install fastback.conf $(DESTDIR)/etc

uninstall:
	rm -rf $(DESTDIR)/bin/{fastback,fastback-unload-receipt}
	rm -rf $(DESTDIR)/etc/fastback.conf

clean:
	rm -rf fastback.o fastback




#
# Maintainer
#

TARFILES=fastback.cpp Makefile fastback.conf fastback-check test fastback-unload-receipt README FIXME INSTALL COPYING fastback-setup-server-anonftp
TARFILENAME=$(NAME)-$(VERSION).tar.gz

# This should be changed to build on dist, when I build a dist
$(TARFILENAME): $(SPECFILE) $(TARFILES)
	rm -rf $(NAME)-$(VERSION)
	mkdir $(NAME)-$(VERSION)
	cp -r $(TARFILES) $(NAME)-$(VERSION)
	tar zcf $@ $(NAME)-$(VERSION)
	rm -rf $(NAME)-$(VERSION)

dist: $(TARFILENAME)

maintainer-clean: clean
	rm -rf $(TARFILENAME) 




#
# RPM specific
# 

ifndef NAME
$(error "You can not run this Makefile without having NAME defined")
endif
SPECFILE=$(NAME).spec

ifneq ($(strip $(wildcard $(SPECFILE))),)

## a base directory where we'll put as much temporary working stuff as we can
WORKDIR := $(shell pwd)

SRCRPMDIR = $(WORKDIR)/SRPMS
SRCRPMDIR_MD5 = $(WORKDIR)/SRPMS_MD5
BUILDDIR = $(WORKDIR)/BUILD
RPMDIR = $(WORKDIR)/RPMS
MOCKDIR = $(WORKDIR)/MOCK

SOURCEDIR = $(WORKDIR)
SPECDIR = $(WORKDIR)

RPM_DEFINES = --define "_sourcedir $(SOURCEDIR)" \
	      --define "_specdir $(SPECDIR)" \
	      --define "_builddir $(BUILDDIR)" \
	      --define "_srcrpmdir $(SRCRPMDIR)" \
	      --define "_rpmdir $(RPMDIR)"

# Initialize the variables that we need, but are not defined
# the version of the package

VER_REL := $(shell rpm $(RPM_DEFINES) -q --qf "%{VERSION} %{RELEASE}\n" --specfile $(SPECFILE)| head -1)

ifneq ($(strip $(VERSION)),$(strip $(word 1, $(VER_REL))))
$(error "VERSION in Makefile does not match VERSION in spec file")
endif
RELEASE := $(word 2, $(VER_REL))

RPM = rpmbuild
RPM_WITH_DIRS = $(RPM) $(RPM_DEFINES)


name-ver-rel:
	@echo $(NAME)-$(VERSION)-$(RELEASE)


srpm: $(SPECFILE) $(NAME)-$(VERSION).tar.gz
	mkdir -p $(SRCRPMDIR) $(BUILDDIR) $(RPMDIR)
	$(RPM_WITH_DIRS) -bs $<

rpm: $(SPECFILE) $(NAME)-$(VERSION).tar.gz
	mkdir -p $(SRCRPMDIR) $(BUILDDIR) $(RPMDIR)
	$(RPM_WITH_DIRS) -ba $(SPECFILE) 2>&1 | tee .build-$(VERSION)-$(RELEASE).log ; exit $${PIPESTATUS[0]}



#
# BUILD SRPMS either with or without the MD5_DEFINES
# 
$(SRCRPMDIR)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm: $(SPECFILE) $(NAME)-$(VERSION).tar.gz
	$(RPM_WITH_DIRS) -bs $<

MD5_DEFINES=--define "_srcrpmdir $(SRCRPMDIR_MD5)" --define "_source_filedigest_algorithm md5" --define "_binary_filedigest_algorithm md5"
$(SRCRPMDIR_MD5)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm: $(SPECFILE) $(NAME)-$(VERSION).tar.gz
	$(RPM_WITH_DIRS) $(MD5_DEFINES) -bs $<



mock-fedora-11-x86_64: $(SRCRPMDIR)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm
	mock $(MOCKARGS) -r fedora-11-x86_64 --resultdir=$(MOCKDIR)/fedora-11-x86_64 rebuild $(SRCRPMDIR)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm

mock-epel-5-x86_64: $(SRCRPMDIR_MD5)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm
	mock $(MOCKARGS) -r epel-5-x86_64 --resultdir=$(MOCKDIR)/epel-5-x86_64 rebuild $(SRCRPMDIR_MD5)/$(NAME)-$(VERSION)-$(RELEASE).src.rpm


rpm-clean: maintainer-clean
	rm -rf $(SRCRPMDIR) $(SRCRPMDIR_MD5) $(BUILDDIR) $(RPMDIR) $(MOCKDIR)

endif